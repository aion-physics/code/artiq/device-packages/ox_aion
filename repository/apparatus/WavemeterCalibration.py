#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from artiq.experiment import *
import logging
import time

"""
This is a sequence for calibrating the HighFinesse wavelength meter.

It scans the wavelength of the Toptica DLPro across a chosen detuning range, and
measures the voltage on a photodiode on the far side of the MOT chamber.
"""
class WavemeterCalibration(EnvExperiment):
    
    def build(self):
        self.setattr_argument("detunings", 
                              Scannable(
                                      RangeScan(-400, 400, 801, randomize=False),
                                      unit="MHz", 
                                      scale=1,
                                      global_min=-1500.0,
                                      global_max=1500.0
                                      ),
                                      tooltip="Frequency detuning of the 461nm lock."
                              )
        self.setattr_device("sampler0")
        self.setattr_device('wavemeter')
        self.setattr_device("sr_oven")
        self.setattr_device("core")
        self.setattr_device("laserloop461")
        self.setattr_argument("repeats", NumberValue(ndecimals=0, step=1, default=3))

        kernel_invariants = {"_channel","_monitor"}
        self._channel = 5
        self._monitor = 7#artiq analog i/p channel


    @kernel
    def measure_pd_voltage(self):
        self.core.break_realtime()
        delay(50*ms)
        self.sampler0.init()
        delay(5*ms)
        self.sampler0.set_gain_mu(self._channel, 0)
        self.sampler0.set_gain_mu(self._monitor, 0)
        delay(5*ms)
        avg_n = 20
        channel_avg = 0.0
        monitor_avg = 0.0
        for i in range(avg_n):
            read_out = [0.0]*8
            self.sampler0.sample(read_out)
            channel_avg = channel_avg + read_out[self._channel]
            monitor_avg = monitor_avg + read_out[self._monitor]
            delay(1*ms)
        
        return ( channel_avg / avg_n, monitor_avg / avg_n )
        
    
    def run(self):
        wavelengths = []
        voltages = []
        spec_voltages = []
        monitor_voltages = []
        min_detuning = min(self.detunings)
        oven_temp = self.sr_oven.get_temperature()
        self.set_dataset("oven_temp_C", oven_temp, broadcast=False)
        for i in range(self.repeats):
            self.laserloop461.set_setpoint_nm(self.get_lambda(min_detuning))
            time.sleep(20)
            for detuning in self.detunings:
                self.laserloop461.set_setpoint_nm(self.get_lambda(detuning))
                time.sleep(1)
                wavelength = self.wavemeter.get_wavelength()
                measurement = self.measure_pd_voltage()
                spec_voltage = measurement[0]
                monitor_voltage = measurement[1]
                voltage = spec_voltage / monitor_voltage #normalise voltage wrt monitor diode
                voltages.append(voltage)
                wavelengths.append(wavelength)
                spec_voltages.append(spec_voltage)
                monitor_voltages.append(monitor_voltage)
                self.set_dataset(_ds_wavelength, wavelengths, broadcast=True, archive=True)
                self.set_dataset(_ds_pd_voltage, voltages, broadcast=True, archive=True)
                self.set_dataset(_ds_spec_voltage, spec_voltages, broadcast=True, archive=True)
                self.set_dataset(_ds_monitor_voltage, monitor_voltages, broadcast=False, archive=True)


    def get_lambda(self, detuning):
        lambda0 = 460.861997
        c = 3e8 * 1e9 * 1e-6 # units of nm/us
        lambdaLock = c / ((c / lambda0) + detuning)
        return lambdaLock

_ds_wavelength = 'wavemeter_calibration_wavelength'
_ds_pd_voltage = 'wavemeter_calibration_voltage'
_ds_spec_voltage = 'spectroscopy_voltage'
_ds_monitor_voltage = 'monitor_diode_voltage'
