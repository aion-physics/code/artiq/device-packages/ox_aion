#!/usr/bin/env python3
import asyncio

from artiq.experiment import *
import time
  
class LaserLoop(EnvExperiment):
    """Laser Loop
    
    Allows setting of simple parameters of the laser loop.
    """
    def build(self):
        self.setattr_device("laserloop461")
        self.setattr_argument("detuning", NumberValue(ndecimals=1, step=1))
        self.setattr_argument("piezo_limit_lower", NumberValue(30, ndecimals=2, step=1))
        self.setattr_argument("piezo_limit_upper", NumberValue(34, ndecimals=2, step=1))
        self.setattr_argument("kP", NumberValue(0, ndecimals=1, step=1))
        self.setattr_argument("kI", NumberValue(-1, ndecimals=1, step=1))
        self.setattr_argument("enabled", BooleanValue(True))
        #todo add piezo limits

    def run(self):
        if self.enabled:
            self.laserloop461.set_loop_limits([self.piezo_limit_lower, self.piezo_limit_upper])
            # todo: need to set actual correct value of wavelength
            lambda0 = 460.861997
            c = 3e8 * 1e9 * 1e-6 # units of nm/us
            lambdaLock = c / ((c / lambda0) + self.detuning)
            self.laserloop461.set_setpoint_nm(lambdaLock)
            self.laserloop461.set_loop_parameters([self.kP, self.kI, 0])
            self.laserloop461.lock()
        else:
            self.laserloop461.unlock()

class LaserSetpoint(EnvExperiment):
    """Laser Setpoint
    
    Change just the setpoint of the 461nm lock.
    """
    def build(self):
        self.setattr_device("laserloop461")
        self.setattr_argument("detuning", NumberValue(ndecimals=1, step=1))

    def run(self):
        # todo: need to set actual correct value of wavelength
        lambda0 = 460.861997
        c = 3e8 * 1e9 * 1e-6 # units of nm/us
        lambdaLock = c / ((c / lambda0) + self.detuning)
        self.laserloop461.set_setpoint_nm(lambdaLock)
        time.sleep(10)
