#!/usr/bin/env python3
import asyncio

from artiq.experiment import *
  
class MotCamera(EnvExperiment):
    
    def build(self):
        self.setattr_device("mot_camera")
        self.setattr_argument("triggered", NumberValue(ndecimals=1, step=1))
        self.setattr_argument("ROI_x", NumberValue(1040, ndecimals=0, step=1)) # was 1100
        self.setattr_argument("ROI_y", NumberValue(800, ndecimals=0, step=1)) # was 800
        self.setattr_argument("ROI_width", NumberValue(240, ndecimals=0, step=1)) # was 240
        self.setattr_argument("ROI_height", NumberValue(240, ndecimals=0, step=1)) # was 240
        self.setattr_argument("triggered", BooleanValue(False))
        self.setattr_argument("save_picture", BooleanValue(False))
        self.setattr_argument("frame_number", NumberValue(1, ndecimals=0, step=1))
        self.setattr_argument("frame_spacing_ms", NumberValue(10, ndecimals=1, step=1))
        self.setattr_argument("exposure_us", NumberValue(1500, ndecimals=0, step=100))
        self.setattr_device("core")
        self.setattr_device("mot_camera_trigger")

    def run(self):

        self.mot_camera.reset()
        self.mot_camera.set_roi(self.ROI_x, self.ROI_y, self.ROI_width, self.ROI_height)
        self.mot_camera.configure_trigger(enabled=self.triggered)

        self.mot_camera.start_acquisition(frame_number=self.frame_number, exposure_us=self.exposure_us)

        self.trigger_camera()


        for i in range(self.frame_number):
            frame = self.mot_camera.get_frame()
            self.set_dataset("mot_image_{}".format(i), frame, broadcast=True, archive=self.save_picture)

        self.mot_camera.end_acquisition()

    @kernel
    def trigger_camera(self):
        self.core.reset()
        delay(10*ms)
        for i in range(self.frame_number):
            with parallel:
                self.mot_camera_trigger.pulse(50*us)
                delay(self.frame_spacing_ms*ms)
        self.core.wait_until_mu(now_mu())
