#!/usr/bin/env python3
import asyncio

from artiq.experiment import *
  
class CameraCal(EnvExperiment):
    
    def build(self):
        self.setattr_device("mot_camera")
        self.setattr_argument("triggered", NumberValue(ndecimals=1, step=1))
        self.setattr_argument("ROI_x", NumberValue(1100, ndecimals=0, step=1))
        self.setattr_argument("ROI_y", NumberValue(800, ndecimals=0, step=1))
        self.setattr_argument("ROI_width", NumberValue(240, ndecimals=0, step=1))
        self.setattr_argument("ROI_height", NumberValue(240, ndecimals=0, step=1))
        self.setattr_argument("triggered", BooleanValue(False))
        self.setattr_argument("save_picture", BooleanValue(False))
        self.setattr_argument("frame_number", NumberValue(100, ndecimals=0, step=1))
        self.setattr_argument("frame_spacing_ms", NumberValue(10, ndecimals=1, step=1))
        self.setattr_argument("start_exposure_us", NumberValue(50, ndecimals=0, step=100))
        self.setattr_argument("exposure_increment_us", NumberValue(25, ndecimals=0, step=100))
        self.setattr_device("core")
        self.setattr_device("mot_camera_trigger")

    def run(self):

        self.mot_camera.reset()
        self.mot_camera.set_roi(self.ROI_x, self.ROI_y, self.ROI_width, self.ROI_height)
        self.mot_camera.configure_trigger(enabled=self.triggered)

        frames = []
        for i in range(self.frame_number):
            t_exp = self.start_exposure_us + i*self.exposure_increment_us
            self.mot_camera.start_acquisition(frame_number=1, exposure_us=t_exp)
            self.trigger_camera()
            frames.append(self.mot_camera.get_frame())
            self.set_dataset("mot_image", frames[-1], broadcast=True, archive=self.save_picture)
            self.mot_camera.end_acquisition()

        self.set_dataset("frames", frames, broadcast=False, archive=True)

    @kernel
    def trigger_camera(self):
        self.core.reset()
        delay(10*ms)
        self.mot_camera_trigger.pulse(50*us)
        delay(self.frame_spacing_ms*ms)
        self.core.wait_until_mu(now_mu())
