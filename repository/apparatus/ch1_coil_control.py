from artiq.experiment import *

"""
Sets the current through two of the chamber 1 coils via the 10V analogue O/P from artiq.

Coil drivers use a 10V max control voltage to set the current through the coils to a maximum of 5 or 10A (controllers 1 and 2 respectively).
"""

class Ch1Coils(EnvExperiment):
    
    def build(self):
        self.setattr_argument("coil1_current", NumberValue(ndecimals=1, step=0.1, default=0.0, scale=1, unit='A', max=5), tooltip="current to go through controlled coil 1")
        self.setattr_argument("coil2_current", NumberValue(ndecimals=1, step=0.1, default=0.0, scale=1, unit='A', max=10), tooltip="current to go through controlled coil 2")
        self.setattr_device('core')
        self.setattr_device('fastino0')
    
    """
    PROBLEM:
    fastino O/P defaults to -10V, so if artiq is restarted -- or the O/P is not actively
    being held at 0V for any other reason -- then a voltage of -10V gets sent out.
    """

    @kernel
    def run(self):
        self.core.reset()
        delay(10*ms)
        
        self.fastino0.init()
        delay(10*ms)

        # mA to A; LDC8040 sensitivity of 0.4 A/V; fastino O/P gets divided by 9.2        
        self.fastino0.set_dac(1, self.coil1_current * 2)
        delay(10*ms)
        self.fastino0.set_dac(7, self.coil2_current * 1)
        delay(10*ms)
        
        delay(2*s)
        self.core.wait_until_mu(now_mu())
