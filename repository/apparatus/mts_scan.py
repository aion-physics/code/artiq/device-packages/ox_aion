#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from artiq.experiment import *
import logging
import time

"""
This is a sequence for performing an MTS scan to precisely find the Sr88 resonance.

It scans the wavelength of the Toptica DLPro across a chosen detuning range, and
measures the voltage on a photodiode on the far side of the chamber, also returning
the MTS signal.
"""

class MTS(EnvExperiment):
    
    def build(self):
        self.setattr_argument("detunings", 
                              Scannable(
                                      RangeScan(-100, 100, 201, randomize=False),
                                      unit="MHz", 
                                      scale=1,
                                      global_min=-1500.0,
                                      global_max=1500.0
                                      ),
                                      tooltip="Frequency detuning of the 461nm lock."
                              )
        self.setattr_device("sampler0")
        self.setattr_device('wavemeter')
        self.setattr_device("core")
        self.setattr_device("laserloop461")
        self.setattr_argument("repeats", NumberValue(ndecimals=0, step=1, default=3))

        #sampler  i/p channels
        kernel_invariants = {"_pd","_mts"}
        self._pd = 6
        self._mts = 4 


    @kernel
    def measure_values(self):
        self.core.break_realtime()
        delay(50*ms)
        self.sampler0.init()
        delay(5*ms)
        self.sampler0.set_gain_mu(self._pd, 0)
        self.sampler0.set_gain_mu(self._mts, 0)
        delay(5*ms)
        avg_n = 20
        pd_avg = 0.0
        mts_avg = 0.0
        for i in range(avg_n):
            read_out = [0.0]*8
            self.sampler0.sample(read_out)
            pd_avg = pd_avg + read_out[self._pd]
            mts_avg = mts_avg + read_out[self._mts]
            delay(1*ms)
        
        return ( pd_avg / avg_n, mts_avg / avg_n )
        
        
    def run(self):
        wavelengths = []
        pd_voltages = []
        mts_voltages = []
        min_detuning = min(self.detunings)
        for i in range(self.repeats):
            self.laserloop461.set_setpoint_nm(self.get_lambda(min_detuning))
            time.sleep(20)
            for detuning in self.detunings:
                self.laserloop461.set_setpoint_nm(self.get_lambda(detuning))
                time.sleep(1)
                wavelength = self.wavemeter.get_wavelength()
                measurement = self.measure_values()
                pd_voltage = measurement[0]
                mts_voltage = measurement[1]
                wavelengths.append(wavelength)
                pd_voltages.append(pd_voltage)
                mts_voltages.append(mts_voltage)
                self.set_dataset(_ds_wavelength, wavelengths, broadcast=True, archive=True)
                self.set_dataset(_ds_pd_voltage, pd_voltages, broadcast=True, archive=True)
                self.set_dataset(_ds_mts_voltage, mts_voltages, broadcast=True, archive=True)


    def get_lambda(self, detuning):
        lambda0 = 460.861997
        c = 3e8 * 1e9 * 1e-6 # units of nm/us
        lambdaLock = c / ((c / lambda0) + detuning)
        return lambdaLock

_ds_wavelength = 'wavemeter_wavelength'
_ds_pd_voltage = 'pd_voltage'
_ds_mts_voltage = 'mts_voltage'
