from artiq.experiment import *
import numpy as np

def build(experiment):
    experiment.setattr_device("sampler0")
    experiment.setattr_device("cooling_a_shutter")
    experiment.setattr_device("cooling_b_shutter")
    experiment.setattr_device("fastino0")

@kernel
def init(experiment):
    experiment.sampler0.init()
    experiment.sampler0.set_gain_mu(1, 0)
    experiment.sampler0.set_gain_mu(3, 0)

@kernel
def measure_beam_voltages(experiment):
    read_out = [0.0]*8
    experiment.sampler0.sample(read_out)
    return ( read_out[1], read_out[3] )

@kernel
def measure_beam_powers(experiment):
    (a_v, b_v) = measure_beam_voltages(experiment)
    return ( a_v / _volt_per_mw_a, b_v / _volt_per_mw_b )

@kernel     
def set_lc_voltage_a(experiment, voltage):
    experiment.fastino0.set_dac(2, voltage)

@kernel     
def set_lc_voltage_b(experiment, voltage):
    experiment.fastino0.set_dac(3, voltage)


# PD calibrated by hand; volts from PD / mW in cooling beam
# Needs to be remeasured after any changes to the laser inputs
_volt_per_mw_a = 0.1160 / 45 # 45mW @ 116.0mV
_volt_per_mw_b = 0.1487 / 54 # 45mW @ 148.7mV


class SetCoolingBeamPowers(EnvExperiment):
    """
    SetCoolingBeamPowers

    Sets the powers of the cooling beams
    """

    def build(self):
        self.setattr_device("core")
        self.setattr_argument("power_a_mw", NumberValue(
            ndecimals=1, step=1, default=50))
        self.setattr_argument("power_b_mw", NumberValue(
            ndecimals=1, step=1, default=50))
        build(self)

    def run(self):
        # Determine the control voltages required for the requested power levels
        voltages = self.get_dataset(_cooling_beam_voltages)
        powers_a = np.array(self.get_dataset(_cooling_beam_a_powers))
        powers_b = np.array(self.get_dataset(_cooling_beam_b_powers))

        # find the index of each power which is closest to the recommended level in mW.
        i_a = np.argmin(np.abs(powers_a - self.power_a_mw))
        i_b = np.argmin(np.abs(powers_b - self.power_b_mw))
        print('Identified control voltages: A={} (prev measured {}mW), B={} (prev measured {}mW)'.format(
            voltages[i_a],
            powers_a[i_a],
            voltages[i_b],
            powers_a[i_b]
            ))

        # set voltages to achieve desired power level.
        self.set_voltages(voltages[i_a], voltages[i_b])

    @kernel
    def set_voltages(self, a, b):
        self.core.reset()
        init(self)
        delay(10*ms)
        set_lc_voltage_a(self, a)
        delay(10*ms)
        set_lc_voltage_b(self, b)


class CalibrateCoolingBeamPower(EnvExperiment):
    """
    CalibrateCoolingBeamPower

    Measures the cooling beam powers as a function of liquid crystal control voltages.
    Saves the calibrations to the global dataset.
    """

    def build(self):
        self.setattr_device("core")
        build(self)

    def run(self):

        # Measure 
        voltages = np.linspace(0, 2, 200)
        self.measure(voltages)

        self.set_dataset(_cooling_beam_voltages, voltages, broadcast=True, archive=True)

    @kernel
    def measure(self, voltages):
        powers_a = [0.0]*200
        powers_b = [0.0]*200
        self.core.reset()
        init(self)
        delay(10*ms)
        set_lc_voltage_a(self, 0.0)
        delay(1*ms)
        set_lc_voltage_b(self, 0.0)
        delay(100*ms)
        for i in range(len(voltages)):
            voltage = voltages[i]
            set_lc_voltage_a(self, voltage)
            delay(1*ms)
            set_lc_voltage_b(self, voltage)
            delay(40*ms)
            (a, b) = measure_beam_powers(self)
            powers_a[i] = a
            powers_b[i] = b
            delay(5*ms)
        self.set_powers_a(powers_a)
        self.set_powers_b(powers_b)

    def set_powers_a(self, powers):
        self.set_dataset(_cooling_beam_a_powers, powers, broadcast=True, archive=True)

    def set_powers_b(self, powers):
        self.set_dataset(_cooling_beam_b_powers, powers, broadcast=True, archive=True)

    def report(self, power):
        print('Cooling A PD={}'.format(power))
    
_cooling_beam_a_powers = "cooling_beam_a_powers"
_cooling_beam_b_powers = "cooling_beam_b_powers"
_cooling_beam_voltages = "cooling_beam_voltages"
