#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from artiq.experiment import *
import logging
import time

"""
Measure Photodiode voltage reading
and HighFinesse wavelength reading
"""

class InterferometryReading(EnvExperiment):

    def build(self):
        self.setattr_device("sampler0")
        self.setattr_device("core")
        kernel_invariants = {"_pd1", "_pd2", "_trigger", "_power"}
        self._pd1 = 0
        self._pd2 = 1
        self._trigger = 2
        self._power = 3
        
        self.f_sample = 10000
        self.T_sample = 3
        self.dt_sample = 1/self.f_sample
        
                
        self.N_sample = int(self.f_sample*self.T_sample)
        
        self.wavelength_list   = [0.0]*self.N_sample 
        self.pd1_vol_list      = [0.0]*self.N_sample 
        self.pd2_vol_list      = [0.0]*self.N_sample 
        self.trigger_vol_list  = [0.0]*self.N_sample 
        self.power_vol_list    = [0.0]*self.N_sample  
        self.buffer = [0.0]*8

    @kernel
    def measure_pd_voltage(self):
        self.core.reset() 
        delay(1*ms)
        
        self.sampler0.init()
        delay(5*ms)
        
        self.sampler0.set_gain_mu(self._pd1, 1)
        self.sampler0.set_gain_mu(self._pd2, 1)
        self.sampler0.set_gain_mu(self._trigger, 0)
        self.sampler0.set_gain_mu(self._power, 1)
        delay(5*ms)

        for i in range(self.N_sample): 
            t0 = now_mu()
            self.sampler0.sample(self.buffer)
            #self.wavelength_list[i] = self.buffer[self._pd1]
            self.pd1_vol_list[i] = self.buffer[self._pd1]
            self.pd2_vol_list[i] = self.buffer[self._pd2]
            self.trigger_vol_list[i] = self.buffer[self._trigger]
            self.power_vol_list[i] = self.buffer[self._power]
            
            # print(self.buffer)
            
            t1 = now_mu()
            delay(self.dt_sample - self.core.mu_to_seconds(t1-t0))
		


        
    def run(self):        
        self.measure_pd_voltage()
        self.set_dataset(_ds_wavelength, self.wavelength_list, broadcast=True, archive=True)
        self.set_dataset(_ds_pd1_voltage, self.pd1_vol_list , broadcast=True, archive=True)
        self.set_dataset(_ds_pd2_voltage, self.pd2_vol_list, broadcast=True, archive=True)
        self.set_dataset(_ds_trigger_voltage, self.trigger_vol_list, broadcast=True, archive=True)
        self.set_dataset(_ds_power_voltage, self.power_vol_list, broadcast=True, archive=True)
        
                


#    def get_lambda(self, detuning):
#        lambda0 = 460.861997
#        c = 3e8 * 1e9 * 1e-6 # units of nm/us
#        lambdaLock = c / ((c / lambda0) + detuning)
#        return lambdaLock


# ds: dataset
_ds_wavelength       = 'interferometry_wavelength'
_ds_pd1_voltage      = 'interferometry_pd1_voltage'
_ds_pd2_voltage      = 'interferometry_pd2_voltage'
_ds_trigger_voltage  = 'interferometry_trigger_voltage'
_ds_power_voltage    = 'interferometry_power_voltage'






























