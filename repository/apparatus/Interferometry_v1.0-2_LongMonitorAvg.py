#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from artiq.experiment import *
import logging
import time

"""
Measure Photodiode voltage reading
"""

class InterferometryLongMonitorAvg(EnvExperiment):

    def build(self):
        self.setattr_device("sampler0")
        self.setattr_device("core")
        
        kernel_invariants = {"_pd1", "_pd2", "_trigger", "_power"}
        self._pd1 = 0
        self._pd2 = 1
        self._trigger = 2
        self._power = 3
        
        self.measurement_type = 'Long Term Monitor'
        self.f_sample_coarse = 10
        self.f_sample_fine = 10 * 1000
        
        self.T_sample = 60 * 10
        
        self.dt_sample_coarse = 1/self.f_sample_coarse
        self.dt_sample_fine = 1/self.f_sample_fine
        
                
        self.N_sample = int(self.f_sample_coarse * self.T_sample)
        self.N_avg = 20
        
        self.time_list         = [0.0]*self.N_sample 
        self.wavelength_list   = [0.0]*self.N_sample
        
        self.pd1_vol_list      = [[0.0]*self.N_avg] * self.N_sample
        self.pd2_vol_list      = [[0.0]*self.N_avg] * self.N_sample
        # self.trigger_vol_list  = [0.0]*self.N_sample 
        self.power_vol_list    = [[0.0]*self.N_avg] * self.N_sample 
        self.buffer = [0.0]*8



    @kernel
    def measure_pd_voltage(self):
        self.core.reset() 
        delay(1*ms)
        
        self.sampler0.init()
        delay(5*ms)
        
        self.sampler0.set_gain_mu(self._pd1, 1)
        self.sampler0.set_gain_mu(self._pd2, 1)
        self.sampler0.set_gain_mu(self._trigger, 0)
        self.sampler0.set_gain_mu(self._power, 1)
        delay(5*ms)

        for i in range(self.N_sample): 
            t0_coarse = now_mu()
            self.time_list[i] = self.core.mu_to_seconds(t0_coarse)
            
            
            for j in range(self.N_avg):
                t2_fine = now_mu()
                
                self.sampler0.sample(self.buffer)
                #self.wavelength_list[i] = self.buffer[self._pd1]
                self.pd1_vol_list[i][j] = self.buffer[self._pd1]
                self.pd2_vol_list[i][j] = self.buffer[self._pd2]
                #self.trigger_vol_list[i][j] = self.buffer[self._trigger]
                self.power_vol_list[i][j] = self.buffer[self._power]
                
                t3_fine = now_mu()
                
                delay(self.dt_sample_fine - 
                      self.core.mu_to_seconds(t3_fine - t2_fine))
                
            
            # print(self.buffer)
            
            t1_coarse = now_mu()
            delay(self.dt_sample_coarse - 
                  self.core.mu_to_seconds(t1_coarse - t0_coarse))
		


        
    def run(self):        
        self.measure_pd_voltage()
        
        self.set_dataset(_ds_measurement_type, self.measurement_type, 
                         broadcast=False, archive=True)
        # self.set_dataset(_ds_wavelength, self.wavelength_list, broadcast=True, archive=True)
        self.set_dataset(_ds_time, self.time_list, broadcast=True, archive=True)
        self.set_dataset(_ds_pd1_voltage, self.pd1_vol_list, broadcast=True, archive=True)
        self.set_dataset(_ds_pd2_voltage, self.pd2_vol_list, broadcast=True, archive=True)
        # self.set_dataset(_ds_trigger_voltage, self.trigger_vol_list, broadcast=True, archive=True)
        self.set_dataset(_ds_power_voltage, self.power_vol_list, broadcast=True, archive=True)
        
        
        pd1_vol_column = [pd1[0]   for pd1 in self.pd1_vol_list]
        pd2_vol_column = [pd2[0]   for pd2 in self.pd2_vol_list]
        
        self.set_dataset(_ds_pd1_voltage_show, pd1_vol_column, broadcast=True, archive=False)
        self.set_dataset(_ds_pd2_voltage_show, pd2_vol_column, broadcast=True, archive=False)
        # self.set_dataset(_ds_trigger_voltage, self.trigger_vol_list, broadcast=True, archive=True)
        # self.set_dataset(_ds_power_voltage, self.power_vol_list, broadcast=True, archive=True)
        


# ds: dataset
_ds_measurement_type = 'interferometry_measurement_type'
_ds_time             = 'interferometry_sampling_time'
# _ds_wavelength       = 'interferometry_wavelength'
_ds_pd1_voltage      = 'interferometry_pd1_voltage'
_ds_pd2_voltage      = 'interferometry_pd2_voltage'
_ds_trigger_voltage  = 'interferometry_trigger_voltage'
_ds_power_voltage    = 'interferometry_power_voltage'

_ds_pd1_voltage_show = 'long_monitor_pd1_voltage'
_ds_pd2_voltage_show = 'long_monitor_pd2_voltage'






























