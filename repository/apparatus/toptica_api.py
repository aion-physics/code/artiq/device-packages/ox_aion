#!/usr/bin/env python3
import asyncio

from artiq.experiment import *
  
class TopticaLaserInterface(EnvExperiment):
    def build(self):
        self.setattr_device("dlc_controller_1")
        self.setattr_device("dlc_controller_2")
        self.setattr_argument("piezo_limit_lower", NumberValue(30, ndecimals=1, step=1))
        self.setattr_argument("piezo_limit_upper", NumberValue(34, ndecimals=1, step=1))
        self.setattr_argument("piezo_voltage", NumberValue(34, ndecimals=5, step=1))

        
    def run(self):
        asyncio.run(self.query_laser())

    async def query_laser(self):
        print("DLC 1 uptime = {}".format(self.dlc_controller_1.get('uptime')))
        print("DLC 2 uptime = {}".format(self.dlc_controller_2.get('uptime')))
        print("DLC 1 laser 1 piezo before = {}".format(self.dlc_controller_1.get('laser1:dl:pc:voltage-set')))
        self.dlc_controller_1.set_piezo_voltage_limits(self.piezo_limit_lower, self.piezo_limit_upper)
        self.dlc_controller_1.set_piezo_voltage(self.piezo_voltage)
        print("DLC 1 laser 1 piezo after = {}".format(self.dlc_controller_1.get('laser1:dl:pc:voltage-set')))
