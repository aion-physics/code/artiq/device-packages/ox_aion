from artiq.experiment import *
from artiq.language.environment import NumberValue, BooleanValue
from artiq.language.scan import RangeScan, Scannable
import numpy as np
import time
from datetime import date
from datetime import datetime


class CameraObserve(EnvExperiment):
    """
    CameraObserve

    Periodically records oven temperature and photographs a chosen ROI.

    The ROI is specified as an input (x and y offsets and ranges), as is the time interval
    between photos. A photo of this ROI is then taken regularly as defined by this interval,
    and the oven temperature logged with each data point.


    Attributes:
        ds_counts: List holding the counts measured so far
        ds_images: List holding the images taken so far
        ds_times: List holding the times measured so far
        ds_temps: List holding the temperatures measured so far
    """

    def build(self):
        self.setattr_device("core")
        self.setattr_device("sr_oven")
        self.setattr_device("mot_camera")
        self.setattr_device("mot_camera_trigger")

        # Arguments for configuring the sweep
        self.setattr_argument("exposure_time_us", NumberValue(
            ndecimals=0, step=10, default=200, unit="us", scale=1), tooltip="Camera exposure time")
        self.setattr_argument("ROI_x", NumberValue(1288, ndecimals=0, step=1))
        self.setattr_argument("ROI_y", NumberValue(1048, ndecimals=0, step=1))
        self.setattr_argument("ROI_width", NumberValue(240, ndecimals=0, step=1))
        self.setattr_argument("ROI_height", NumberValue(240, ndecimals=0, step=1))
        self.setattr_argument("save_images", BooleanValue(False))
        self.setattr_argument("interval_s", NumberValue(
            ndecimals=0, step=1, default=30, unit="s", scale=1), tooltip="interval between photos in seconds")
        self.setattr_argument("duration_mins", NumberValue(
            ndecimals=0, step=10, default=720, unit="min", scale=1), tooltip="total duration of scan in minutes")
        self.camera_gain = 0
        

    def run(self):
        self.core.reset()
        self.initialise()
        self.mot_camera.reset()
        self.mot_camera.configure_trigger(enabled=True)
        self.mot_camera.set_roi(self.ROI_x, self.ROI_y, self.ROI_width, self.ROI_height)
        
        # calculate the number of measurements to make
        repeats = int(np.ceil(self.duration_mins*60/self.interval_s))
        
        for repeat in range(repeats):
            self.do_measurement()
            self.set_dataset(_ds_counts, self.ds_counts, broadcast=True)
            self.set_dataset(_ds_times, self.ds_times, broadcast=True)
            self.set_dataset(_ds_temps, self.ds_temps, broadcast=False)
            time.sleep(self.interval_s)

        if self.save_images:
            self.set_dataset(_ds_image, self.ds_images, broadcast=False)

        

    def initialise(self):
        """
        Initialise the experiment
        """
        self.ds_counts = []
        self.ds_images = []
        self.ds_times = []
        self.ds_temps = []
        self.set_dataset("camera_gain", self.camera_gain, broadcast=False)
        self.set_dataset(
            "exposure_time", self.exposure_time_us, broadcast=False)



    def do_measurement(self):
        """
        Takes a single measurement (photo and temperature reading).
        """
        
	# reading the oven temperature
        print('trying to read oven temperature now...')
        oven_temp = self.sr_oven.get_temperature()
        temps = []
        temps.append(oven_temp)
	
	# beginning camera acquisition
        self.mot_camera.start_acquisition(frame_number=1, exposure_us=self.exposure_time_us)
        while not self.mot_camera.get_acquisition_in_progress():
            time.sleep(0.1)  # wait until camera acquisition is in progress
            
        self.trigger_camera()

        # taking the photo
        images = []
        try:
            print('trying to take a photo now...')
            images.append(self.mot_camera.get_frame())
        except:
            raise Exception("Unable to retrieve expected number of camera frames. This may be caused by an exposure time that is too long, or a frame spacing that is too small, so that triggers are dropped.")
        self.mot_camera.end_acquisition()

        # broadcast the most recent image so we can keep an eye on things
        self.set_dataset("mot_image", images[-1], broadcast=True, archive=False)

	# save the number of counts in the image	
        counts = []
        counts.append(np.sum(images))
            
	# note the time (in the format YYYYMMDD_HHMMSS)
        times = []
        today = date.today().strftime('%Y%m%d')
        now = datetime.now().strftime('%H%M%S')
        times.append(today+'_'+now)

        # save the data
        self.ds_counts += counts
        self.ds_images += images
        self.ds_times += times
        self.ds_temps += temps
        
        
    @kernel
    def trigger_camera(self):
        self.core.reset()
        delay(10*ms)
        self.mot_camera_trigger.pulse(50*us)
        self.core.wait_until_mu(now_mu())


_ds_image = "images"
_ds_counts = "counts"
_ds_times = "times"
_ds_temps = "temps"
