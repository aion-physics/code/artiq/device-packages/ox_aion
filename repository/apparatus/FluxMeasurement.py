#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from artiq.experiment import *
import logging
import time

"""
This is a sequence for calibrating the HighFinesse wavelength meter.

It scans the wavelength of the Toptica DLPro across a chosen detuning range, and
measures the voltage on a photodiode on the far side of the MOT chamber.
"""
class FluxMeasurement(EnvExperiment):
    
    def build(self):
        self.setattr_argument("detunings", 
                              Scannable(
                                      RangeScan(95.0, 125.0, 61, randomize=False),
                                      unit="MHz", 
                                      scale=1,
                                      global_min=0.0,
                                      global_max=200.0
                                      ),
                                      tooltip="Push beam AOM modulation frequency."
                              )
        self.setattr_device("sampler0")
        self.setattr_device("sr_oven")
        self.setattr_device("core")
        self.setattr_device("urukul1_cpld")
        self.setattr_device("urukul1_ch2")
        self.setattr_device("ttl_urukul1_sw2")
        #self.setattr_device("laserloop461")
        self.setattr_argument("repeats", NumberValue(ndecimals=0, step=1, default=3))

        kernel_invariants = {"_channel","_monitor"}
        self._channel = 5
        self._monitor = 7#artiq analog i/p channel

    @kernel
    def measure_pd_voltage(self):
        self.core.break_realtime()
        delay(50*ms)
        self.sampler0.init()
        delay(5*ms)
        self.sampler0.set_gain_mu(self._channel, 0)
        self.sampler0.set_gain_mu(self._monitor, 0)
        delay(5*ms)
        avg_n = 20
        channel_avg = 0.0
        monitor_avg = 0.0
        for i in range(avg_n):
            read_out = [0.0]*8
            self.sampler0.sample(read_out)
            channel_avg = channel_avg + read_out[self._channel]
            monitor_avg = monitor_avg + read_out[self._monitor]
            delay(1*ms)
        
        return ( channel_avg / avg_n, monitor_avg / avg_n )
    @kernel
    def initialise_dds(self):
        self.core.reset()
        delay(10*ms)
        self.urukul1_cpld.init()
        
        #init blue channel 2 DDS
        delay(100*ms)
        self.urukul1_ch2.init()
        self.urukul1_cpld.set_att(2, 0.0)
        
    @kernel    
    def set_frequency(self, detuning):
        delay(1*s)
        self.urukul1_ch2.set(detuning*MHz, amplitude=0.22)
        delay(50*ms)
    
    def run(self):
        self.initialise_dds()
        voltages = []
        spec_voltages = []
        monitor_voltages = []
        oven_temp = self.sr_oven.get_temperature()
        self.set_dataset("oven_temp_C", oven_temp, broadcast=False)
        for i in range(self.repeats):
            time.sleep(20)
            for detuning in self.detunings:
                #self.laserloop461.set_setpoint_nm(self.get_lambda(detuning))
                self.set_frequency(detuning)
                time.sleep(10)
                
                measurement = self.measure_pd_voltage()
                spec_voltage = measurement[0]
                monitor_voltage = measurement[1]
                voltage = spec_voltage / monitor_voltage #normalise voltage wrt monitor diode
                voltages.append(voltage)
                spec_voltages.append(spec_voltage)
                monitor_voltages.append(monitor_voltage)
                
                self.set_dataset(_ds_detuning, self.detunings, broadcast=True, archive=True)
                self.set_dataset(_ds_pd_voltage, voltages, broadcast=True, archive=True)
                self.set_dataset(_ds_spec_voltage, spec_voltages, broadcast=True, archive=True)
                self.set_dataset(_ds_monitor_voltage, monitor_voltages, broadcast=False, archive=True)


_ds_detuning= 'flux_measurement_detuning'
_ds_pd_voltage = 'flux_measurement_voltage'
_ds_spec_voltage = 'transmitted_voltage'
_ds_monitor_voltage = 'normalisation_voltage'
