"""
The startup kernel for the experiment.
"""
from artiq.experiment import *
        
class Startup(EnvExperiment):
    
    def build(self):
        self.setattr_device("core")
        self.setattr_device("fastino0")
        self.setattr_device("urukul0_cpld")
        self.setattr_device("urukul1_cpld")
        self.setattr_device("urukul0_ch0")
        self.setattr_device("urukul0_ch1")
        self.setattr_device("urukul0_ch2")
        self.setattr_device("urukul0_ch3")
        self.setattr_device("urukul1_ch0")
        self.setattr_device("urukul1_ch1")
        self.setattr_device("urukul1_ch2")
        self.setattr_device("urukul1_ch3")
        
    @kernel
    def run(self):
        
        # Reset the core
        self.core.reset()
        
        # Initialise the Fastino
        self.fastino0.init()

        # I tried to initialise Urukuls - but the fibre link wont be ready in time. Doesn't work.
        delay(10*ms)
        self.urukul0_cpld.init()
        delay(10*ms)
        self.urukul1_cpld.init()
        delay(10*ms)
        self.urukul0_ch0.init()
        self.urukul0_ch1.init()
        self.urukul0_ch2.init()
        self.urukul0_ch3.init()
        self.urukul1_ch0.init()
        self.urukul1_ch1.init()
        self.urukul1_ch2.init()
        self.urukul1_ch3.init()
        delay(100*ms)

        self.core.wait_until_mu(now_mu())
        
        
        
