#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from artiq.experiment import *
import logging

"""
This is the 'master' sequence for characterising the 3D MOT output.

It does not take any experiments in-and-of itself, but it sets up various scans
and spawns child experiments to change different 3D MOT parameters.

For each set of parameters, the following sequences are performed:
    * The output is scanned using the excitation beam and imaged.
    * If 'measure_tof', a time-of-flight sweep is taken.
    
The following parameters can be scanned:
    * Detuning of the cooling beams.
    * Quadrupole coil current.
    * Intensity of the cooling beams.
    
Each launched sub-experiment records all parameter information.
"""
class Characterise3DMOT(EnvExperiment):
    
    def build(self):
        self.setattr_argument("cooling_beam_powers", 
                              Scannable(
                                      RangeScan(40, 80, 5, randomize=True),
                                      unit="mW", 
                                      scale=1,
                                      global_min=0.0,
                                      global_max=200.0
                                      ),
                                      tooltip="Power in cross cooling beams."
                              )
        self.setattr_argument("detunings", 
                              Scannable(
                                      RangeScan(0, -100, 5, randomize=True),
                                      unit="MHz", 
                                      scale=1,
                                      global_min=-1000.0,
                                      global_max=1000.0
                                      ),
                                      tooltip="Frequency detuning of the 461nm lock."
                              )
        self.setattr_device('scheduler')
        self.setattr_argument("loading_times", 
                              Scannable(
                                      RangeScan(-0.00, 0.0035, 8, randomize=True),
                                      unit="ms",
                                      global_min=-50,
                                      global_max=50)
                              )
        self.setattr_argument(
            "frame_spacing",
            NumberValue(ndecimals=1, step=1, default=4e-3, unit="ms"),
            group="Multiframe",
            tooltip="Spacing between successive camera pictures."
            )
        self.setattr_argument(
            "frame_number",
            NumberValue(ndecimals=0, step=1, scale=1, default=18, type="int"),
            group="Multiframe",
            tooltip="Total number of frames to acquire.",
            )
        self.setattr_argument("repeats", NumberValue(ndecimals=0, step=1, default=15))
        self.setattr_argument("exposure_time_us", NumberValue(ndecimals=0, step=10, default=150, unit="us", scale=1), tooltip="Camera exposure time")
        
        
    def run(self):
        for detuning in self.detunings:
            self.schedule_set_wavemeter_lock(detuning)
            for power in self.cooling_beam_powers:
                self.schedule_calibrate_cooling_power()
                self.schedule_set_cooling_power(power)
                self.schedule_loading_time()
        
        

    
    def schedule_loading_time(self):
        """
        Schedules an experiment to charaterise the output, using the specified
        arguments
        """
        experiment = {
                "log_level": logging.WARNING,
                "repo_rev": None,
                "file": "mot/LoadingTime3DMOT.py",
                "class_name": 'LoadingTime3DMOT',
                "arguments": { 
                        'repeats': self.repeats,
                        'exposure_time_us': self.exposure_time_us,
                        'loading_times': self.loading_times.describe(),
                        'frame_number': self.frame_number,
                        'frame_spacing': self.frame_spacing,
                        'save_images': False
                        }
                }
        self.scheduler.submit("main", experiment, 0, None, False)
           
    """
    Schedules an experiment to set the cooling power to the desired fraction
    """
    def schedule_set_cooling_power(self, power):
        experiment = {
                "log_level": logging.WARNING,
                "repo_rev": None,
                "file": 'apparatus/coolingbeams.py',
                "class_name": 'SetCoolingBeamPowers',
                "arguments": { 'power_a_mw': power, 'power_b_mw': power }
                }
        self.scheduler.submit("main", experiment, 0, None, False)

    """
    Schedules an experiment to set the cooling power to the desired fraction
    """
    def schedule_set_wavemeter_lock(self, detuning):
        experiment = {
                "log_level": logging.WARNING,
                "repo_rev": None,
                "file": 'apparatus/laserloop.py',
                "class_name": 'LaserSetpoint',
                "arguments": { 'detuning': detuning }
                }
        self.scheduler.submit("main", experiment, 0, None, False)
                
    """
    Schedules an experiment to calibrate the cooling attenuator
    """
    def schedule_calibrate_cooling_power(self):
        experiment = {
                "log_level": logging.WARNING,
                "repo_rev": None,
                "file": 'apparatus/coolingbeams.py',
                "class_name": 'CalibrateCoolingBeamPower',
                "arguments": dict()
                }
        self.scheduler.submit("main", experiment, 0, None, False)
            

