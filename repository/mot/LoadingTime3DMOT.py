from artiq.experiment import *
from artiq.language.environment import NumberValue, BooleanValue
from artiq.language.scan import RangeScan, Scannable
import numpy as np
import time

#from .. import apparatus
from apparatus import coolingbeams

class LoadingTime3DMOT(EnvExperiment):
    """
    LoadingTime3DMOT

    Measures the time taken to load the 3D MOT.

    The MOT is configured with the cooling beams initially off. The cooling beams
    are turned on, and after a known duration the atom number in the MOT is measured.


    Attributes:
        ds_counts: List holding the counts measured so far
        ds_images: List holding the images taken so far
        ds_times: List holding the times measured so far
    """

    def build(self):
        self.setattr_device("core")
        self.setattr_device("sr_oven")
        self.setattr_device("mot_camera_trigger")
        self.setattr_device("wavemeter")
        self.setattr_device("mot_camera")
        self.setattr_device("cooling_a_shutter")
        self.setattr_device("cooling_b_shutter")

        # Arguments for configuring the sweep
        self.setattr_argument("loading_times",
                              Scannable(
                                  RangeScan(-0.01, -0.006, 5, randomize=True),
                                  unit="ms",
                                  global_min=-50,
                                  global_max=50)
                              )
        self.setattr_argument("repeats", NumberValue(
            ndecimals=0, step=1, default=3))
        self.setattr_argument("exposure_time_us", NumberValue(
            ndecimals=0, step=10, default=200, unit="us", scale=1), tooltip="Camera exposure time")
        self.setattr_argument("save_images", BooleanValue(False))
        self.setattr_argument(
            "frame_spacing",
            NumberValue(ndecimals=1, step=1, default=5e-3, unit="ms"),
            group="Multiframe",
            tooltip="Spacing between successive camera pictures."
            )
        self.setattr_argument(
            "frame_number",
            NumberValue(ndecimals=0, step=1, scale=1, default=10, type="int"),
            group="Multiframe",
            tooltip="Total number of frames to acquire.",
            )
        self.camera_gain = 0
        coolingbeams.build(self)

    def run(self):
        self.core.reset()
        self.initialise()
        self.mot_camera.reset()
        self.mot_camera.configure_trigger(enabled=True)
        for repeat in range(self.repeats):
            for loading_time in self.loading_times:
                self.do_measurement(loading_time)
            self.set_dataset(_ds_counts, self.ds_counts, broadcast=True)
            self.set_dataset(_ds_loading_times, self.ds_times, broadcast=True)
            self.aggregate_datasets()

        if self.save_images:
            self.set_dataset(_ds_image, self.ds_images, broadcast=False)

        

    def initialise(self):
        """
        Initialise the experiment
        """
        self.ds_counts = []
        self.ds_images = []
        self.ds_times = []
        self.ds_pd_cooling = []
        self.set_dataset("camera_gain", self.camera_gain, broadcast=False)
        self.set_dataset(
            "exposure_time", self.exposure_time_us, broadcast=False)
        
        oven_temp = self.sr_oven.get_temperature()
        self.set_dataset("oven_temp_C", oven_temp, broadcast=False)
        wavelength = self.wavemeter.get_wavelength()
        self.set_dataset("lambda", wavelength, broadcast=False)

        powers = self.measure_cooling_beam_powers()
        self.set_dataset("cooling_beam_a_power", powers[0], broadcast=True)
        self.set_dataset("cooling_beam_b_power", powers[1], broadcast=True)

    def do_measurement(self, wait_time):
        """
        Takes a single measurement at the wait time.
        """
        self.mot_camera.start_acquisition(
            frame_number=self.frame_number, exposure_us=self.exposure_time_us)
        while not self.mot_camera.get_acquisition_in_progress():
            time.sleep(0.1)  # wait until camera acquisition is in progress
        self.measure_atom_number(wait_time)

        # Get images, determine atom number
        images = []
        for i in range(self.frame_number):
            try:
                images.append(self.mot_camera.get_frame())
            except:
                raise Exception("Unable to retrieve expected number of camera frames. This may be caused by an exposure time that is too long, or a frame spacing that is too small, so that triggers are dropped.")

        self.mot_camera.end_acquisition()

        # Broadcast picture so we can keep an eye on things.
        self.set_dataset("mot_image", images[-1], broadcast=True, archive=False)

        counts = []
        wait_times = []
        for i in range(self.frame_number):
            counts.append(np.sum(images[i]))
            wait_times.append(wait_time+i*self.frame_spacing)

        # Save data
        self.ds_counts += counts
        self.ds_images += images
        self.ds_times += wait_times

    @kernel
    def cooling_beams_on(self):
        self.cooling_a_shutter.on()
        self.cooling_b_shutter.on()

    @kernel
    def cooling_beams_off(self):
        self.cooling_a_shutter.off()
        self.cooling_b_shutter.off()

    @kernel
    def measure_cooling_beam_powers(self):
        self.core.reset()
        delay(10*ms)
        coolingbeams.init(self)
        delay(10*ms)
        powers = coolingbeams.measure_beam_powers(self)
        self.core.wait_until_mu(now_mu())
        return powers

    """
    Measure the atom number present after a given loading time.
    """
    @kernel
    def measure_atom_number(self, loading_time):
        self.core.reset()
        delay(2*ms)
        self.cooling_beams_off()
        off_duration = 100*ms
        with parallel:
            with sequential:
                delay(off_duration)
                self.cooling_beams_on()
            with sequential:
                delay(off_duration+loading_time)
                for i in range(self.frame_number):
                    with parallel:
                        self.mot_camera_trigger.pulse(100*us)
                        delay(self.frame_spacing)
        delay(50*ms)
        self.cooling_beams_on()
        self.core.wait_until_mu(now_mu())

    def aggregate_datasets(self):
        loading_times = np.array(self.ds_times)
        counts = np.array(self.ds_counts)

        unique_lts = [lt for lt in np.unique(
            loading_times) if not np.isnan(lt)]
        grouped_counts = [np.array(counts[(lt == loading_times)])
                          for lt in unique_lts]
        avgs = [np.average(c) for c in grouped_counts]
        error = [np.std(c) / pow(np.size(c), 0.5) for c in grouped_counts]

        self.set_dataset(_ds_loading_times_avg, unique_lts, broadcast=True)
        self.set_dataset(_ds_counts_avg, avgs, broadcast=True)
        self.set_dataset(_ds_counts_err, error, broadcast=True)


_ds_image = "images"
_ds_counts = "counts"
_ds_loading_times = "loading_times"
_ds_counts_avg = "counts_avg"
_ds_loading_times_avg = "loading_times_avg"
_ds_counts_err = "counts_err"
