from artiq.experiment import *
from artiq.master.scheduler import Scheduler;
import time;

from apparatus import coolingbeams

"""An experiment for logging quantities that require kernel access.

Run low priority - will yield to higher importance experiments."""
class KernelIdle(EnvExperiment):
    
    def build(self):
        self.setattr_device("core")
        self.setattr_device("scheduler")
        self.setattr_device("influx_logger")
        coolingbeams.build(self)
    
    @kernel
    def measure_cooling_power(self):
        self.core.reset()
        delay(10*ms)
        coolingbeams.init(self)
        delay(10*ms)
        powers = coolingbeams.measure_beam_powers(self)
        self.core.wait_until_mu(now_mu())
        self.log_cooling_power(powers)

    def log_cooling_power(self, cooling_power):
        self.influx_logger.write(fields={
            "cooling_beam_a_power": float(cooling_power[0]),
            "cooling_beam_b_power": float(cooling_power[1])
            },
            tags={"chamber": "1"},
            timestamp=time.time()
            )

    def run(self):

        while (True): # lol

            start = time.time()

            self.measure_cooling_power()

            # yield execution to higher priority threads.
            # self.scheduler.pause()

            end = time.time()
            duration = end - start
            if duration < 1:
                time.sleep(1 - duration)
