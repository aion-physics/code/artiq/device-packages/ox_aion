"""
Logs properties of the lasers and records them on influxDB.
"""
from warnings import catch_warnings
from artiq.experiment import *
from artiq.master.scheduler import Scheduler;
import time;


class LogLasers(EnvExperiment):
    
    def build(self):
        self.setattr_device("influx_logger")
        self.setattr_device("dlc_controller_1")
        self.setattr_device("dlc_controller_2")
        self.setattr_device('wavemeter')
        self.setattr_device('sr_oven')
        
    def run(self):

        while (True): # lol

            start = time.time()
            laser_temp_461 = self.dlc_controller_1.get('laser1:dl:tc:temp-act')
            laser_2_T = self.dlc_controller_2.get('laser1:dl:tc:temp-act')
            laser_3_T = self.dlc_controller_2.get('laser2:dl:tc:temp-act')
            
            self.influx_logger.write(fields={
                    "laser_461_T": float(laser_temp_461),
                    "laser_2_T": float(laser_2_T),
                    "laser_3_T": float(laser_3_T)
                },
                tags={"equipment": "toptica"},
                timestamp=time.time()
                )

            try:
                wavelength_461 = self.wavemeter.get_wavelength()
                self.influx_logger.write(fields={
                        "wavelength_461": float(wavelength_461),
                    },
                    tags={"equipment": "high-finesse"},
                    timestamp=time.time()
                )
            except:
                pass # we professional now


            sr_oven_T = self.sr_oven.get_temperature()
            self.influx_logger.write(fields={
                    "oven_T": float(sr_oven_T),
                },
                tags={"equipment": "sr_oven"},
                timestamp=time.time()
            )

            end = time.time()
            duration = end - start
            if duration < 1:
                time.sleep(1 - duration)

            # yield execution to higher priority threads.
            #Scheduler.pause()
