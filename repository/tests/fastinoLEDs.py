#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Set the values of the zotino LEDs
"""
from artiq.experiment import *
        
class FastinoLEDS(EnvExperiment):
    
    def build(self):
        self.setattr_device("core")
        self.setattr_device("fastino0")
        self.setattr_argument("LED_state", BooleanValue(default=True))
        
    @kernel
    def run(self):
        
        # Reset the core
        self.core.reset()
        delay(10*ms)
        
        for i in range(256):
            self.fastino0.set_leds(i)
            delay(30*ms)
        
        delay(1000*ms)
        self.fastino0.set_leds(0)
        
        delay(10*ms)
        self.core.wait_until_mu(now_mu())
        
