"""
The idle sequence for the experiment.
"""
from artiq.experiment import *
  
class SimpleTTLs(EnvExperiment):
    
    def build(self):
        self.setattr_device("core")
        self.setattr_device("ttl1")
        self.setattr_device("ttl2")
        self.setattr_device("ttl16")
        
    @kernel
    def run(self):
        self.core.reset()        
        
        delay(100*ms)
        self.ttl2.off()
        delay(100*ms)
        self.ttl2.on()

        with parallel:
            self.ttl1.pulse(100*ms)
            with sequential:
                for i in range(8):
                    self.ttl16.pulse(1*ms)
                    delay(1*ms)

        delay(100*ms)
        
        # Wait for idle to finish     
        self.core.wait_until_mu(now_mu())
