from artiq.experiment import *
from artiq.language.environment import NumberValue, BooleanValue
import numpy as np;

"""
Tests the ADC by reading from AI0.

Samples the data and produces datasets for t and V.
These datasets can be plotted as an applet.
"""
class TestADC(EnvExperiment):
    
    def build(self):
        self.setattr_device("core")
        self.setattr_device("sampler0")
        self.setattr_device("fastino0")
        self.setattr_argument("channel", NumberValue(ndecimals=0, step=1))
    
    @kernel
    def run(self):
        
        num_samples = 1024
        self.init_dataset(num_samples)
        
        self.core.break_realtime()
        delay(50*ms)
        self.sampler0.init()
        for i in range(8):
            self.sampler0.set_gain_mu(i, 0)
        
        
        data = [0.0]*num_samples
        read_out = [0.0]*8
        dt = 1
        
        # Make a crappy scope.
        for i in range(num_samples):
            self.fastino0.set_dac(0, i/1025*10-5)
            delay(dt*ms)
            self.sampler0.sample(read_out)
            delay(dt*ms)
            data[i] = read_out[self.channel]
        
        self.fastino0.set_dac(0, 0.0)
        self.save_dataset(dt, data)
        
    """
    Creates the dataset that will be used to save scope data.
    """
    def init_dataset(self, num_samples):
        self.set_dataset("sampler_t", np.full(num_samples, np.nan), broadcast=True)
        self.set_dataset("sampler_v", np.full(num_samples, np.nan), broadcast=True)
        
    """
    Saves the generated dataset.
    """
    def save_dataset(self, dt, data):
        t = [1e-3 * dt * x for x in range(len(data))]
        for i in range(len(data)):
            self.mutate_dataset("sampler_t", i, t[i])
            self.mutate_dataset("sampler_v", i, data[i])
        
        