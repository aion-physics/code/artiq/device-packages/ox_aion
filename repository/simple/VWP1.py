from artiq.experiment import *

class SetVWP1(EnvExperiment):
    
    def build(self):
        self.setattr_argument("voltage", NumberValue(ndecimals=1, step=0.1, default=2, scale=1, unit='V'), tooltip="VWP Voltage")
        self.setattr_device('core')
        self.setattr_device('fastino0')
    
    @kernel
    def run(self):
        self.core.reset()
        delay(10*ms)
        self.fastino0.init()
        delay(10*ms)
        self.fastino0.set_dac(1,self.voltage)

        delay(2*s)
        self.core.wait_until_mu(now_mu())

