#!/usr/bin/env python3
import asyncio

from artiq.experiment import *
  
class WavemeterAPI(EnvExperiment):
    def build(self):
        self.setattr_device("wavemeter")
        
    def run(self):
        wavelength = self.wavemeter.get_wavelength()
        print("Measured wavelength={}".format(wavelength))