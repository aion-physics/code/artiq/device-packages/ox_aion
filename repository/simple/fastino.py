"""
Test the fastino analog output
"""
from artiq.experiment import *
  
class Fastino(EnvExperiment):
    
    def build(self):
        self.setattr_device("core")
        self.setattr_device("ttl0")
        self.setattr_device("fastino0")
        self.setattr_argument("channel", NumberValue(ndecimals=0, step=1))
        
    @kernel
    def run(self):
        self.core.reset()        
        
        delay(100*ms)

        self.ttl0.pulse(1*ms)
        
        for i in range(-10,11, 1):
            self.fastino0.set_dac(self.channel, i*0.99)
            delay(1*ms)

        self.fastino0.set_dac(self.channel, 0.0)
        delay(100*ms)
        
        # Wait for idle to finish     
        self.core.wait_until_mu(now_mu())
