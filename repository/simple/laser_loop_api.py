#!/usr/bin/env python3
import asyncio

from artiq.experiment import *
  
class LaserLoopAPI(EnvExperiment):
    def build(self):
        self.setattr_device("laserloop461")
        
    def run(self):
        self.laserloop461.ping()