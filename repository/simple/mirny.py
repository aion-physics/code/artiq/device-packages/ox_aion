"""
Test an urukul in standalone configuration.
"""
from artiq.experiment import *
  
class Mirny(EnvExperiment):
    
    def build(self):
        self.setattr_device("core")
        self.setattr_device("mirny0_cpld")
        self.setattr_device("mirny0_ch0")
        self.setattr_device("mirny0_ch1")
        self.setattr_device("mirny0_ch2")
        self.setattr_device("mirny0_ch3")
        self.setattr_device("ttl_mirny0_sw0")
        self.setattr_device("ttl_mirny0_sw1")
        self.setattr_device("ttl_mirny0_sw2")
        self.setattr_device("ttl_mirny0_sw3")

        
    @kernel
    def run(self):
        self.core.reset()
        delay(100*ms)

        self.mirny0_cpld.init()

        delay(10*ms)
        self.mirny0_ch0.init()
        self.mirny0_ch1.init()
        self.mirny0_ch2.init()
        self.mirny0_ch3.init()

        self.mirny0_ch0.set_frequency(60*MHz)
        self.mirny0_ch1.set_frequency(60*MHz)
        self.mirny0_ch2.set_frequency(60*MHz)
        self.mirny0_ch3.set_frequency(60*MHz)

        delay(10*ms)
        self.ttl_mirny0_sw0.pulse(1*s)
        self.ttl_mirny0_sw1.pulse(1*s)
        self.ttl_mirny0_sw2.pulse(1*s)
        self.ttl_mirny0_sw3.pulse(1*s)
        
        # Wait for idle to finish     
        self.core.wait_until_mu(now_mu())
