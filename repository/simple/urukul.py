"""
Test an urukul in standalone configuration.
"""
from artiq.experiment import *
  
class UrukulStandalone(EnvExperiment):
    
    def build(self):
        self.setattr_device("core")
        self.setattr_device("urukul0_cpld")
        self.setattr_device("urukul1_cpld")
        self.setattr_device("urukul0_ch0")
        self.setattr_device("urukul0_ch1")
        self.setattr_device("urukul0_ch2")
        self.setattr_device("urukul0_ch3")
        self.setattr_device("urukul1_ch0")
        self.setattr_device("urukul1_ch1")
        self.setattr_device("urukul1_ch2")
        self.setattr_device("urukul1_ch3")

        self.setattr_device("ttl_urukul0_sw0")
        self.setattr_device("ttl_urukul0_sw1")
        self.setattr_device("ttl_urukul0_sw2")
        self.setattr_device("ttl_urukul0_sw3")
        self.setattr_device("ttl_urukul1_sw0")
        self.setattr_device("ttl_urukul1_sw1")
        self.setattr_device("ttl_urukul1_sw2")
        self.setattr_device("ttl_urukul1_sw3")

        
    @kernel
    def run(self):
        self.core.reset()
        delay(100*ms)

        self.urukul0_cpld.init()

        delay(10*ms)
        self.urukul1_cpld.init()

        delay(10*ms)
        self.urukul0_ch0.init()
        self.urukul0_ch1.init()
        self.urukul0_ch2.init()
        self.urukul0_ch3.init()
        self.urukul1_ch0.init()
        self.urukul1_ch1.init()
        self.urukul1_ch2.init()
        self.urukul1_ch3.init()
        delay(100*ms)

        self.urukul1_ch3.set(10*MHz, 0.1)
        self.urukul1_cpld.set_att(3, 1.0)
        delay(10*ms)
        self.ttl_urukul1_sw3.pulse(1000*ms)
        
        # Wait for idle to finish     
        self.core.wait_until_mu(now_mu())
