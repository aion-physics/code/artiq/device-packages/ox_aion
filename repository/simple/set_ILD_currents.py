from artiq.experiment import *

"""
Shifts the current on the diode controller box by a chosen amount.

The analog modulation input on the Thorlabs LDC8040 adds a current to the value already
set which is proportional to the voltage applied at this input. This script uses this
MOD input to shift the current by a chosen amount.
"""

class ShiftILDCurrents(EnvExperiment):
    
    def build(self):
        self.setattr_argument("ILD1_shift", NumberValue(ndecimals=1, step=0.1, default=0.0, scale=1, unit='mA'), tooltip="current shift for ILD Box 1")
        self.setattr_argument("ILD2_shift", NumberValue(ndecimals=1, step=0.1, default=0.0, scale=1, unit='mA'), tooltip="current shift for ILD Box 2")
        self.setattr_argument("ILD3_shift", NumberValue(ndecimals=1, step=0.1, default=0.0, scale=1, unit='mA'), tooltip="current shift for ILD Box 3")
        self.setattr_device('core')
        self.setattr_device('fastino0')
    
    """
    PROBLEM:
    fastino O/P defaults to -10V, so if artiq is restarted -- or the O/P is not actively
    being held at 0V for any other reason -- then a voltage of -10V gets sent out. This
    becomes 1.09V after the divider, corresponding to a shift of -435mA. This will
    effectively turn the diodes off for most current values.
    """

    @kernel
    def run(self):
        self.core.reset()
        delay(10*ms)
        
        self.fastino0.init()
        delay(10*ms)

        # mA to A; LDC8040 sensitivity of 0.4 A/V; fastino O/P gets divided by 9.2        
        self.fastino0.set_dac(4, self.ILD1_shift / 1000 / 0.4 * 9.2)
        delay(10*ms)
        self.fastino0.set_dac(5, self.ILD2_shift / 1000 / 0.4 * 9.2)
        delay(10*ms)
        self.fastino0.set_dac(6, self.ILD3_shift / 1000 / 0.4 * 9.2)
        
        delay(2*s)
        self.core.wait_until_mu(now_mu())
