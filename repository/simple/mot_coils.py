from artiq.experiment import *

"""
Sets the current on the MOT quadrupole coils to the desired level.

Current specified in A.
"""

"""
@portable
def current(experiment, current):
    voltage = _current_to_magnapower_control_voltage(current)
    experiment.zotino3.write_dac(0, voltage)
    experiment.zotino3.load()
    
@portable    
def _current_to_magnapower_control_voltage(current):
    return current / 90 * 10;

@portable
def build(experiment):
    experiment.setattr_device("fastino0")

_mot_coil_current_ds = "log_coil_current"
"""

"""
A simple experiment that sets the current in the mot coils.
"""
class SetMOTCoilCurrent(EnvExperiment):
    
    def build(self):
        self.setattr_argument("current", NumberValue(ndecimals=0, step=1, default=60, scale=1, unit='A'), tooltip="MOT Quadrupole coil current")
        self.setattr_device('core')
        self.setattr_device('fastino0')
    
    @kernel
    def run(self):
        self.core.reset()
        delay(10*ms)
        self.fastino0.init()
        delay(10*ms)
        self.fastino0.set_dac(0,self.current / 90 * 10)

        #current(self, self.current)
        delay(2*s)
        #self.set_dataset(_mot_coil_current_ds, float(self.current), broadcast=True)
        self.core.wait_until_mu(now_mu())

#def get_requested_current(experiment):
    """Get the intended value of the quadrupole coil current, in A."""
#    return experiment.get_dataset(_mot_coil_current_ds)
