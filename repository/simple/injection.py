"""
Set the frequency of an urukul channel
"""
from artiq.experiment import *
  
class InjectionLockTest(EnvExperiment):
    
    def build(self):
        self.setattr_device("core")
        self.setattr_device("urukul1_cpld")
        self.setattr_device("urukul1_ch2")

        
    @kernel
    def run(self):
        self.core.reset()
        delay(100*ms)

        # Assumes urukuls init'ed previously.
        
        # give it a bit of time to complete
        delay(10*ms)


        # set frequency and amplitude of channel outputs     
        for i in range(100):
            for freq in range(10):
                self.urukul1_ch2.set((110+freq)*MHz)
                delay(300*ms)
        
        # Wait for idle to finish     
        self.core.wait_until_mu(now_mu())
