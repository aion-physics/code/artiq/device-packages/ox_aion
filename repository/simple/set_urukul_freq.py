"""
Set the frequency of an urukul channel
"""
from artiq.experiment import *
  
class SetUrukulFrequency(EnvExperiment):
    
    def build(self):
        self.setattr_device("core")
        self.setattr_device("urukul1_cpld")
        self.setattr_device("urukul0_cpld")

        self.setattr_device("urukul1_ch0")
        self.setattr_device("ttl_urukul1_sw0")
        
        self.setattr_device("urukul1_ch1")
        self.setattr_device("ttl_urukul1_sw1")

        self.setattr_device("urukul1_ch2")
        self.setattr_device("ttl_urukul1_sw2")

        self.setattr_device("urukul1_ch3")
        self.setattr_device("ttl_urukul1_sw3")

        self.setattr_device("urukul0_ch0")
        self.setattr_device("ttl_urukul0_sw0")
        
        self.setattr_device("urukul0_ch1")
        self.setattr_device("ttl_urukul0_sw1")
        
        self.setattr_device("urukul0_ch2")
        self.setattr_device("ttl_urukul0_sw2")
        
        self.setattr_device("urukul0_ch3")
        self.setattr_device("ttl_urukul0_sw3")
        
        self.setattr_argument('Blue1_amplitude', 
                              NumberValue(ndecimals=2, step=0.01, default=0.21, min=0.0, max=1.0),
                              tooltip="amplitude of the dds")
        self.setattr_argument('Blue1_frequency', 
                              NumberValue(ndecimals=1, step=0.5, default=115, min=10.0, max=350.0),
                              tooltip="frequency of the dds")

        self.setattr_argument('Blue2_amplitude', 
                              NumberValue(ndecimals=2, step=0.01, default=0.21, min=0.0, max=1.0),
                              tooltip="amplitude of the dds")
        self.setattr_argument('Blue2_frequency', 
                              NumberValue(ndecimals=1, step=0.5, default=115, min=10.0, max=350.0),
                              tooltip="frequency of the dds")
        
        self.setattr_argument('Blue3_amplitude', 
                              NumberValue(ndecimals=2, step=0.01, default=0.21, min=0.0, max=1.0),
                              tooltip="amplitude of the dds")
        self.setattr_argument('Blue3_frequency', 
                              NumberValue(ndecimals=1, step=0.5, default=135, min=10.0, max=350.0),
                              tooltip="frequency of the dds")
        
        self.setattr_argument('Blue4_amplitude', 
                              NumberValue(ndecimals=2, step=0.01, default=0.21, min=0.0, max=1.0),
                              tooltip="amplitude of the dds")
        self.setattr_argument('Blue4_frequency', 
                              NumberValue(ndecimals=1, step=0.5, default=150, min=10.0, max=350.0),
                              tooltip="frequency of the dds")
        
        self.setattr_argument('Red1_amplitude', 
                              NumberValue(ndecimals=2, step=0.01, default=0.26, min=0.0, max=1.0),
                              tooltip="amplitude of the dds")
        self.setattr_argument('Red1_frequency', 
                              NumberValue(ndecimals=1, step=0.5, default=100, min=80.0, max=400.0),
                              tooltip="frequency of the dds")

        self.setattr_argument('Red2_amplitude', 
                              NumberValue(ndecimals=2, step=0.01, default=0.26, min=0.0, max=1.0),
                              tooltip="amplitude of the dds")
        self.setattr_argument('Red2_frequency', 
                              NumberValue(ndecimals=1, step=0.5, default=350, min=300.0, max=400.0),
                              tooltip="frequency of the dds")

        self.setattr_argument('Red3_amplitude', 
                              NumberValue(ndecimals=2, step=0.01, default=0.56, min=0.0, max=1.0),
                              tooltip="amplitude of the dds")
        self.setattr_argument('Red3_frequency', 
                              NumberValue(ndecimals=1, step=0.5, default=350, min=300.0, max=400.0),
                              tooltip="frequency of the dds")

        self.setattr_argument('Red4_amplitude', 
                              NumberValue(ndecimals=2, step=0.01, default=0.26, min=0.0, max=1.0),
                              tooltip="amplitude of the dds")
        self.setattr_argument('Red4_frequency', 
                              NumberValue(ndecimals=1, step=0.5, default=100, min=75.0, max=125.0),
                              tooltip="frequency of the dds")

        
    @kernel
    def run(self):
        self.core.reset()
        
        # init the blue urukul cpld
        self.urukul1_cpld.init()
        #self.urukul0_cpld.init()
        
        delay(50*ms)
        
        # init blue channel 0 DDS
        self.urukul1_ch0.init()
        delay(10*ms)
        
        # init blue channel 1 DDS
        self.urukul1_ch1.init()
        delay(10*ms)

        # init blue channel 2 DDS
        self.urukul1_ch2.init()
        delay(10*ms)
        
        # init blue channel 3 DDS
        self.urukul1_ch3.init()
        delay(10*ms)
        
        
        # init the red urukul cpld
        self.urukul0_cpld.init()
        
        delay(50*ms)
	
        # init red channel 0 DDS
        self.urukul0_ch0.init()
        delay(10*ms)
	
        # init red channel 1 DDS
        self.urukul0_ch1.init()
        delay(10*ms)
        
        # init red channel 2 DDS
        self.urukul0_ch2.init()
        delay(10*ms)
        
        # init red channel 3 DDS
        self.urukul0_ch3.init()
        delay(10*ms)
        
	
        # set attenuation of channels to 0
        self.urukul1_cpld.set_att(0, 0.0)
        self.urukul1_cpld.set_att(1, 0.0)
        self.urukul1_cpld.set_att(2, 0.0)
        self.urukul1_cpld.set_att(3, 0.0)
        self.urukul0_cpld.set_att(0, 0.0)
        self.urukul0_cpld.set_att(1, 0.0)
        
        # set attentuation of channels to Booster HL
        #self.urukul0_cpld.set_att(1, 6.*dB)
        self.urukul0_cpld.set_att(2, 3.*dB)
        self.urukul0_cpld.set_att(3, 3.*dB)
        
        # turn on the rf switches
        self.ttl_urukul1_sw0.on()
        self.ttl_urukul1_sw1.on()
        self.ttl_urukul1_sw2.on()
        self.ttl_urukul1_sw3.on()
        self.ttl_urukul0_sw0.on()
        self.ttl_urukul0_sw1.on()
        self.ttl_urukul0_sw2.on()
        self.ttl_urukul0_sw3.on()
 
         # give it a bit of time to complete
        delay(10*ms)
               
        # set frequency and amplitude of channel outputs
        self.urukul1_ch0.set(self.Blue1_frequency*MHz, amplitude=self.Blue1_amplitude)
        self.urukul1_ch1.set(self.Blue2_frequency*MHz, amplitude=self.Blue2_amplitude)        
        self.urukul1_ch2.set(self.Blue3_frequency*MHz, amplitude=self.Blue3_amplitude)
        self.urukul1_ch3.set(self.Blue4_frequency*MHz, amplitude=self.Blue4_amplitude)
        self.urukul0_ch0.set(self.Red1_frequency*MHz, amplitude=self.Red1_amplitude)
        self.urukul0_ch1.set(self.Red2_frequency*MHz, amplitude=self.Red2_amplitude)
        self.urukul0_ch2.set(self.Red3_frequency*MHz, amplitude=self.Red3_amplitude)
        self.urukul0_ch3.set(self.Red4_frequency*MHz, amplitude=self.Red4_amplitude)

        # give it a bit of time to complete
        delay(10*ms)
        
        
        # Wait for idle to finish     
        #self.core.wait_until_mu(now_mu())
