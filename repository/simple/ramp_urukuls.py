"""
Set the frequency of an urukul channel
"""
from artiq.experiment import *
  
class RampUrukulFrequency(EnvExperiment):
    
    def build(self):
        self.setattr_device("core")
        self.setattr_device("urukul1_cpld")
        self.setattr_device("urukul1_ch0")
        self.setattr_device("urukul1_ch1")
        self.setattr_device("urukul1_ch2")
        self.setattr_device("urukul1_ch3")
#        self.setattr_device("ttl_urukul0_sw0")
        self.setattr_argument('amplitude', 
                              NumberValue(ndecimals=2, step=0.01, default=0.5, min=0.0, max=1.0))
        self.setattr_argument('centre_frequency', 
                              NumberValue(ndecimals=1, step=0.5, default=115, min=100.0, max=150.0))
        self.setattr_argument('bandwidth', 
                              NumberValue(ndecimals=1, step=0.5, default=10, min=1.0, max=30.0))

        
    @kernel
    def run(self):
        self.core.reset()
        delay(100*ms)

        while (True):
            for i in range(20):
                # give it a bit of time to complete
                delay(1000*ms)

                freq = ((i * 1.0 - 10.0) / 20.0 * self.bandwidth + self.centre_frequency)*MHz
                self.urukul1_ch0.set(freq, amplitude=self.amplitude)
                delay(1*ms)
                """
                self.urukul1_ch1.set(freq, amplitude=self.amplitude)
                delay(1*ms)
                self.urukul1_ch2.set(freq, amplitude=self.amplitude)
                delay(1*ms)
                self.urukul1_ch3.set(freq, amplitude=self.amplitude)
                delay(1*ms)
                """
        # Wait for idle to finish     
        self.core.wait_until_mu(now_mu())
