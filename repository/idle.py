"""
The idle sequence for the experiment.
"""
from artiq.experiment import *
  
class Idle(EnvExperiment):
    
    def build(self):
        self.setattr_device("core")
        
    @kernel
    def run(self):
        self.core.reset()        
        
        delay(1*s)
        
        # Wait for idle to finish        
        self.core.wait_until_mu(now_mu())
