
# Autogenerated for the kaslimain variant
core_addr = "10.179.78.30" # updated 2024-01-09 from 10.255.13.52

device_db = {
    "core": {
        "type": "local",
        "module": "artiq.coredevice.core",
        "class": "Core",
        "arguments": {"host": core_addr, "ref_period": 1e-09, "target": "rv32g"},
    },
    "core_log": {
        "type": "controller",
        "host": "::1",
        "port": 1068,
        "command": "aqctl_corelog -p {port} --bind {bind} " + core_addr
    },
    "core_cache": {
        "type": "local",
        "module": "artiq.coredevice.cache",
        "class": "CoreCache"
    },
    "core_dma": {
        "type": "local",
        "module": "artiq.coredevice.dma",
        "class": "CoreDMA"
    },

    "i2c_switch0": {
        "type": "local",
        "module": "artiq.coredevice.i2c",
        "class": "I2CSwitch",
        "arguments": {"address": 0xe0}
    },
    "i2c_switch1": {
        "type": "local",
        "module": "artiq.coredevice.i2c",
        "class": "I2CSwitch",
        "arguments": {"address": 0xe2}
    },
}

# master peripherals

device_db["ttl0"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x000000},
}

device_db["ttl1"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x000001},
}

device_db["ttl2"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x000002},
}

device_db["ttl3"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x000003},
}

device_db["ttl4"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x000004},
}

device_db["ttl5"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x000005},
}

device_db["ttl6"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x000006},
}

device_db["ttl7"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x000007},
}

device_db["ttl8"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x000008},
}

device_db["ttl9"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x000009},
}

device_db["ttl10"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x00000a},
}

device_db["ttl11"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x00000b},
}

device_db["ttl12"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x00000c},
}

device_db["ttl13"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x00000d},
}

device_db["ttl14"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x00000e},
}

device_db["ttl15"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x00000f},
}

device_db["ttl16"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x000010},
}

device_db["ttl17"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x000011},
}

device_db["ttl18"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x000012},
}

device_db["ttl19"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x000013},
}

device_db["ttl20"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x000014},
}

device_db["ttl21"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x000015},
}

device_db["ttl22"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x000016},
}

device_db["ttl23"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x000017},
}

device_db["ttl24"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x000018},
}

device_db["ttl25"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x000019},
}

device_db["ttl26"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x00001a},
}

device_db["ttl27"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x00001b},
}

device_db["ttl28"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x00001c},
}

device_db["ttl29"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x00001d},
}

device_db["ttl30"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x00001e},
}

device_db["ttl31"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x00001f},
}

device_db["fastino0"] = {
    "type": "local",
    "module": "artiq.coredevice.fastino",
    "class": "Fastino",
    "arguments": {"channel": 0x000020}
}

device_db["spi_mirny0"]={
    "type": "local",
    "module": "artiq.coredevice.spi2",
    "class": "SPIMaster",
    "arguments": {"channel": 0x000021}
}

device_db["ttl_mirny0_sw0"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x000022}
}

device_db["ttl_mirny0_sw1"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x000023}
}

device_db["ttl_mirny0_sw2"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x000024}
}

device_db["ttl_mirny0_sw3"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x000025}
}

device_db["mirny0_ch0"] = {
    "type": "local",
    "module": "artiq.coredevice.adf5356",
    "class": "ADF5356",
    "arguments": {
        "channel": 0,
        "sw_device": "ttl_mirny0_sw0",
        "cpld_device": "mirny0_cpld",
    }
}

device_db["mirny0_ch1"] = {
    "type": "local",
    "module": "artiq.coredevice.adf5356",
    "class": "ADF5356",
    "arguments": {
        "channel": 1,
        "sw_device": "ttl_mirny0_sw1",
        "cpld_device": "mirny0_cpld",
    }
}

device_db["mirny0_ch2"] = {
    "type": "local",
    "module": "artiq.coredevice.adf5356",
    "class": "ADF5356",
    "arguments": {
        "channel": 2,
        "sw_device": "ttl_mirny0_sw2",
        "cpld_device": "mirny0_cpld",
    }
}

device_db["mirny0_ch3"] = {
    "type": "local",
    "module": "artiq.coredevice.adf5356",
    "class": "ADF5356",
    "arguments": {
        "channel": 3,
        "sw_device": "ttl_mirny0_sw3",
        "cpld_device": "mirny0_cpld",
    }
}

device_db["mirny0_cpld"] = {
    "type": "local",
    "module": "artiq.coredevice.mirny",
    "class": "Mirny",
    "arguments": {
        "spi_device": "spi_mirny0",
        "refclk": 100000000.0,
        "clk_sel": "xo"
    },
}
# DEST#1 peripherals

device_db["spi_sampler0_adc"] = {
    "type": "local",
    "module": "artiq.coredevice.spi2",
    "class": "SPIMaster",
    "arguments": {"channel": 0x010000}
}
device_db["spi_sampler0_pgia"] = {
    "type": "local",
    "module": "artiq.coredevice.spi2",
    "class": "SPIMaster",
    "arguments": {"channel": 0x010001}
}
device_db["ttl_sampler0_cnv"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x010002},
}
device_db["sampler0"] = {
    "type": "local",
    "module": "artiq.coredevice.sampler",
    "class": "Sampler",
    "arguments": {
        "spi_adc_device": "spi_sampler0_adc",
        "spi_pgia_device": "spi_sampler0_pgia",
        "cnv_device": "ttl_sampler0_cnv"
    }
}

device_db["eeprom_urukul0"] = {
    "type": "local",
    "module": "artiq.coredevice.kasli_i2c",
    "class": "KasliEEPROM",
    "arguments": {"port": "EEM2"}
}

device_db["spi_urukul0"] = {
    "type": "local",
    "module": "artiq.coredevice.spi2",
    "class": "SPIMaster",
    "arguments": {"channel": 0x010003}
}

device_db["ttl_urukul0_sync"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLClockGen",
    "arguments": {"channel": 0x010004, "acc_width": 4}
}

device_db["ttl_urukul0_io_update"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x010005}
}

device_db["ttl_urukul0_sw0"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x010006}
}

device_db["ttl_urukul0_sw1"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x010007}
}

device_db["ttl_urukul0_sw2"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x010008}
}

device_db["ttl_urukul0_sw3"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x010009}
}

device_db["urukul0_cpld"] = {
    "type": "local",
    "module": "artiq.coredevice.urukul",
    "class": "CPLD",
    "arguments": {
        "spi_device": "spi_urukul0",
        "sync_device": "ttl_urukul0_sync",
        "io_update_device": "ttl_urukul0_io_update",
        "refclk": 100000000.0,
        "clk_sel": 0
    }
}

device_db["urukul0_ch0"] = {
    "type": "local",
    "module": "artiq.coredevice.ad9910",
    "class": "AD9910",
    "arguments": {
        "pll_n": 40,
        "chip_select": 4,
        "cpld_device": "urukul0_cpld",
        "sw_device": "ttl_urukul0_sw0",
        "pll_vco": 5,
        "sync_delay_seed": "eeprom_urukul0:64",
        "io_update_delay": "eeprom_urukul0:64"
    }
}

device_db["urukul0_ch1"] = {
    "type": "local",
    "module": "artiq.coredevice.ad9910",
    "class": "AD9910",
    "arguments": {
        "pll_n": 40,
        "chip_select": 5,
        "cpld_device": "urukul0_cpld",
        "sw_device": "ttl_urukul0_sw1",
        "pll_vco": 5,
        "sync_delay_seed": "eeprom_urukul0:68",
        "io_update_delay": "eeprom_urukul0:68"
    }
}

device_db["urukul0_ch2"] = {
    "type": "local",
    "module": "artiq.coredevice.ad9910",
    "class": "AD9910",
    "arguments": {
        "pll_n": 40,
        "chip_select": 6,
        "cpld_device": "urukul0_cpld",
        "sw_device": "ttl_urukul0_sw2",
        "pll_vco": 5,
        "sync_delay_seed": "eeprom_urukul0:72",
        "io_update_delay": "eeprom_urukul0:72"
    }
}

device_db["urukul0_ch3"] = {
    "type": "local",
    "module": "artiq.coredevice.ad9910",
    "class": "AD9910",
    "arguments": {
        "pll_n": 40,
        "chip_select": 7,
        "cpld_device": "urukul0_cpld",
        "sw_device": "ttl_urukul0_sw3",
        "pll_vco": 5,
        "sync_delay_seed": "eeprom_urukul0:76",
        "io_update_delay": "eeprom_urukul0:76"
    }
}

device_db["eeprom_urukul1"] = {
    "type": "local",
    "module": "artiq.coredevice.kasli_i2c",
    "class": "KasliEEPROM",
    "arguments": {"port": "EEM4"}
}

device_db["spi_urukul1"] = {
    "type": "local",
    "module": "artiq.coredevice.spi2",
    "class": "SPIMaster",
    "arguments": {"channel": 0x01000a}
}

device_db["ttl_urukul1_sync"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLClockGen",
    "arguments": {"channel": 0x01000b, "acc_width": 4}
}

device_db["ttl_urukul1_io_update"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x01000c}
}

device_db["ttl_urukul1_sw0"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x01000d}
}

device_db["ttl_urukul1_sw1"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x01000e}
}

device_db["ttl_urukul1_sw2"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x01000f}
}

device_db["ttl_urukul1_sw3"] = {
    "type": "local",
    "module": "artiq.coredevice.ttl",
    "class": "TTLOut",
    "arguments": {"channel": 0x010010}
}

device_db["urukul1_cpld"] = {
    "type": "local",
    "module": "artiq.coredevice.urukul",
    "class": "CPLD",
    "arguments": {
        "spi_device": "spi_urukul1",
        "sync_device": "ttl_urukul1_sync",
        "io_update_device": "ttl_urukul1_io_update",
        "refclk": 100000000.0,
        "clk_sel": 0
    }
}

device_db["urukul1_ch0"] = {
    "type": "local",
    "module": "artiq.coredevice.ad9910",
    "class": "AD9910",
    "arguments": {
        "pll_n": 40,
        "chip_select": 4,
        "cpld_device": "urukul1_cpld",
        "sw_device": "ttl_urukul1_sw0",
        "pll_vco": 5,
        "sync_delay_seed": "eeprom_urukul1:64",
        "io_update_delay": "eeprom_urukul1:64"
    }
}

device_db["urukul1_ch1"] = {
    "type": "local",
    "module": "artiq.coredevice.ad9910",
    "class": "AD9910",
    "arguments": {
        "pll_n": 40,
        "chip_select": 5,
        "cpld_device": "urukul1_cpld",
        "sw_device": "ttl_urukul1_sw1",
        "pll_vco": 5,
        "sync_delay_seed": "eeprom_urukul1:68",
        "io_update_delay": "eeprom_urukul1:68"
    }
}

device_db["urukul1_ch2"] = {
    "type": "local",
    "module": "artiq.coredevice.ad9910",
    "class": "AD9910",
    "arguments": {
        "pll_n": 40,
        "chip_select": 6,
        "cpld_device": "urukul1_cpld",
        "sw_device": "ttl_urukul1_sw2",
        "pll_vco": 5,
        "sync_delay_seed": "eeprom_urukul1:72",
        "io_update_delay": "eeprom_urukul1:72"
    }
}

device_db["urukul1_ch3"] = {
    "type": "local",
    "module": "artiq.coredevice.ad9910",
    "class": "AD9910",
    "arguments": {
        "pll_n": 40,
        "chip_select": 7,
        "cpld_device": "urukul1_cpld",
        "sw_device": "ttl_urukul1_sw3",
        "pll_vco": 5,
        "sync_delay_seed": "eeprom_urukul1:76",
        "io_update_delay": "eeprom_urukul1:76"
    }
}

device_db["suservo0_ch0"] = {
    "type": "local",
    "module": "artiq.coredevice.suservo",
    "class": "Channel",
    "arguments": {"channel": 0x010011, "servo_device": "suservo0"}
}

device_db["suservo0_ch1"] = {
    "type": "local",
    "module": "artiq.coredevice.suservo",
    "class": "Channel",
    "arguments": {"channel": 0x010012, "servo_device": "suservo0"}
}

device_db["suservo0_ch2"] = {
    "type": "local",
    "module": "artiq.coredevice.suservo",
    "class": "Channel",
    "arguments": {"channel": 0x010013, "servo_device": "suservo0"}
}

device_db["suservo0_ch3"] = {
    "type": "local",
    "module": "artiq.coredevice.suservo",
    "class": "Channel",
    "arguments": {"channel": 0x010014, "servo_device": "suservo0"}
}

device_db["suservo0_ch4"] = {
    "type": "local",
    "module": "artiq.coredevice.suservo",
    "class": "Channel",
    "arguments": {"channel": 0x010015, "servo_device": "suservo0"}
}

device_db["suservo0_ch5"] = {
    "type": "local",
    "module": "artiq.coredevice.suservo",
    "class": "Channel",
    "arguments": {"channel": 0x010016, "servo_device": "suservo0"}
}

device_db["suservo0_ch6"] = {
    "type": "local",
    "module": "artiq.coredevice.suservo",
    "class": "Channel",
    "arguments": {"channel": 0x010017, "servo_device": "suservo0"}
}

device_db["suservo0_ch7"] = {
    "type": "local",
    "module": "artiq.coredevice.suservo",
    "class": "Channel",
    "arguments": {"channel": 0x010018, "servo_device": "suservo0"}
}

device_db["suservo0"] = {
    "type": "local",
    "module": "artiq.coredevice.suservo",
    "class": "SUServo",
    "arguments": {
        "channel": 0x010019,
        "pgia_device": "spi_sampler1_pgia",
        "cpld_devices": ['urukul2_cpld', 'urukul3_cpld'],
        "dds_devices": ['urukul2_dds', 'urukul3_dds']
    }
}

device_db["spi_sampler1_pgia"] = {
    "type": "local",
    "module": "artiq.coredevice.spi2",
    "class": "SPIMaster",
    "arguments": {"channel": 0x01001a}
}
#####################################################

# trying to get sampler1 working the way sampler0 does




###################################################
device_db["spi_urukul2"] = {
    "type": "local",
    "module": "artiq.coredevice.spi2",
    "class": "SPIMaster",
    "arguments": {"channel": 0x01001b}
}
device_db["urukul2_cpld"] = {
    "type": "local",
    "module": "artiq.coredevice.urukul",
    "class": "CPLD",
    "arguments": {
        "spi_device": "spi_urukul2",
        "refclk": 125000000.0,
        "clk_sel": 0
    }
}
device_db["urukul2_dds"] = {
    "type": "local",
    "module": "artiq.coredevice.ad9910",
    "class": "AD9910",
    "arguments": {
        "pll_n": 40,
        "chip_select": 3,
        "cpld_device": "urukul2_cpld",
        "pll_vco": 5
    }
}

device_db["spi_urukul3"] = {
    "type": "local",
    "module": "artiq.coredevice.spi2",
    "class": "SPIMaster",
    "arguments": {"channel": 0x01001c}
}
device_db["urukul3_cpld"] = {
    "type": "local",
    "module": "artiq.coredevice.urukul",
    "class": "CPLD",
    "arguments": {
        "spi_device": "spi_urukul3",
        "refclk": 125000000.0,
        "clk_sel": 0
    }
}
device_db["urukul3_dds"] = {
    "type": "local",
    "module": "artiq.coredevice.ad9910",
    "class": "AD9910",
    "arguments": {
        "pll_n": 40,
        "chip_select": 3,
        "cpld_device": "urukul3_cpld",
        "pll_vco": 5
    }
}
