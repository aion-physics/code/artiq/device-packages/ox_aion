from device_db_ox.device_db_base import device_db

# We now extend the device_db to include others things such as Network Device Support Packages (NDSPs)

# Add drivers for the toptica lasers
device_db.update(
    dlc_controller_1={
        "type": "controller",
        "host": "::1",
        "port": 13371,
        "command": "aqctl_laser --port {port} --device 10.255.13.61"
    },
    dlc_controller_2={
        "type": "controller",
        "host": "::1",
        "port": 13372,
        "command": "aqctl_laser --port {port} --device 10.255.13.62"
    },
    wavemeter={
        "type": "controller",
        "host": "10.255.13.47",
        "port": 3272,
    },
    laserloop461={
        "type": "controller",
        "host": "::1",
        "port": 13373,
        "command": "run_laser_loop --port {port} --wavemeter-ip 10.255.13.47 --laser-ip ::1 --wavemeter-port 3272 --laser-port 13371 -v"
    },
    influx_logger={
        "type": "controller",
        "host": "::1",
        "port": 13375,
        "command": "artiq_influx_generic --port {port} --bind {bind} --baseurl-db http://allxdaq22:8086 --database aion",
    },
    sr_oven={
        "type": "controller",
        "host": "::1",
        "port": 13376,
        "command": "run_oven_controller --port {port} --bind {bind} -d 10.179.78.25" # updated 2023-12-18 from 10.255.13.64
    },
    mot_camera={
        "type": "controller",
        "host": "::1",
        "port": 8762
    },
    mot_camera_trigger="ttl1",
    cooling_a_shutter="ttl2",
    cooling_b_shutter="ttl3",
)
