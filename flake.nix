{
  description = "ARTIQ environment for the Oxford lab";

  inputs.artiq.url = "git+https://gitlab.com/aion-physics/code/artiq/artiq_fork";
  
  inputs.nixpkgs.follows = "artiq/nixpkgs";
  inputs.sipyco.follows = "artiq/sipyco";

  # Grafana integration
  inputs.artiq_influx_generic.url = "gitlab:charlesbaynham/artiq_influx_generic";
  inputs.artiq_influx_generic.inputs.sipyco.follows = "sipyco";
  inputs.artiq_influx_generic.inputs.nixpkgs.follows = "nixpkgs";

  inputs.lasersdk-artiq.url = "git+https://gitlab.com/aion-physics/code/artiq/drivers/lasersdk-artiq";
  inputs.lasersdk-artiq.inputs.nixpkgs.follows = "nixpkgs";
  inputs.lasersdk-artiq.inputs.sipyco.follows = "sipyco";
  inputs.laser-loop.url = "git+https://gitlab.com/foot-group/laserloop";
  inputs.laser-loop.inputs.sipyco.follows = "sipyco";

  # Ox oven controller
  inputs.aion-oven-controller.url = "gitlab:foot-group/aion-oven-controller";
  inputs.aion-oven-controller.inputs.nixpkgs.follows = "nixpkgs";
  inputs.aion-oven-controller.inputs.sipyco.follows = "sipyco";

  outputs = { self, nixpkgs, artiq, lasersdk-artiq, sipyco, laser-loop, artiq_influx_generic, aion-oven-controller }:
    let
      pkgs = import nixpkgs { system = "x86_64-linux"; };
    in rec {
      devShell.x86_64-linux = pkgs.mkShell {
        name = "ox-artiq-dev-shell";
        buildInputs = [
          (pkgs.python3.withPackages(ps: [ 
            artiq.packages.x86_64-linux.artiq	
            ps.jsonschema
            lasersdk-artiq.packages.x86_64-linux.toptica-lasersdk
            lasersdk-artiq.packages.x86_64-linux.lasersdk-artiq
            laser-loop.packages.x86_64-linux.laser-loop
            aion-oven-controller.packages.x86_64-linux.aion-oven-controller
            artiq_influx_generic.packages.x86_64-linux.artiq_influx_generic
          ]))
          artiq.packages.x86_64-linux.openocd-bscanspi
        ];
        shellHook = ''
echo Entering Oxford AION experiment shell...
export PYTHONPATH=~/ox_aion/repository
'';
      };
    };
}
